"use strict"

function requestInteger(msg,min,max){
    let number;
    do{
        number = parseInt(prompt(msg,1));
    }while(
        isNaN(number) || 
            number < min || 
            number > max          
    );//rebloucle si n'est pas un INT ou compris entre 1 et 3
    
    return number;// renvoie le nombre entré par l'utilisateur
}

//récupérer un nombre "aléatoire" entre deux valeurs
function getRandomInteger(min, max){
    return Math.floor(Math.random() * (max - min + 1) + min);
}
// renvoie l'attaque du dragon en fonction de la difficulté et l'armure du joueur
function computeDragonDamagePoint(difficulty,armorRatio){
    // LET difficulty;
    switch(difficulty){
        case 1:// facile
            game.dragonAttack = Math.floor(getRandomInteger(10, 20) * armorRatio);// dommage du dragon ingligé au chevalier en facile
            break;
        case 2:// normal
            game.dragonAttack = Math.floor(getRandomInteger(20, 30) * armorRatio);// dommage du dragon ingligé au chevalier en facile
            break;
        case 3:// difficile
            game.dragonAttack = Math.floor(getRandomInteger(30, 20) * armorRatio);// dommage du dragon ingligé au chevalier en facile
            break;                      
    }
    //renvoie l'attaque du dragon
    return game.dragonAttack;
    //return console.log("La difficulté et le ratio "+difficulty+' '+ armorRatio);
            
}

function computePlayerDamagePoint(sword,swordRatio){
    
    switch(sword){
        case 1://épée en bois
            game.playerAttack = Math.floor(getRandomInteger(10, 20) * swordRatio);// dommage du dragon ingligé au chevalier en facile
            break;
        case 2://épée en acier
            game.playerAttack = Math.floor(getRandomInteger(20, 30) * swordRatio);// dommage du dragon ingligé au chevalier en facile
            break;
        case 3://difficile
            game.playerAttack = Math.floor(getRandomInteger(30, 20) * swordRatio);// dommage du dragon ingligé au chevalier en facile
            break;                      
    }
    //renvoie l'attaque du dragon
    return game.playerAttack;
}

function displayFight(lifeDragon,lifePlayer,roundNbr,msgFight){
    // ecrit dans la div game
    const DIV = document.querySelector('#game');

    ////////////////////////////////////////////////
    // Affichage du numéro de round dans une balise H3
    let titleRound = document.createElement('h3');
    let roundNumberContent = document.createTextNode(" ------------------ Tour : " +roundNbr+ " ------------------");
    titleRound.appendChild(roundNumberContent);
    DIV.appendChild(titleRound);
    ////////////////////////////////////////////////
    //Affichage du texte du combat
    let pContentMsgFight = document.createElement('p');
    pContentMsgFight.appendChild(msgFight);
    DIV.appendChild(pContentMsgFight);
    ////////////////////////////////////////////////
}