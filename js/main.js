"use strict"

function startGame() {
    let game = initGame();
    game = gameLoop(game);
    showWinner(game);
}

function initGame(){
    //init de l'objet game
    game = new Object();

    game.difficulty = requestInteger("Choisissez la difficulté : \n1.Facile - 2.Normal - 3.Difficile",1,3);
    game.sword = requestInteger("Choisissez l'épée : \n1.Bois - 2.Acier - 3.Escalibur",1,3);
    game.armor = requestInteger("Choisissez l'armure : \n1.Cuivre - 2.Fer - 3.Magique",1,3);
    
    // attribution des pv par difficulté
    switch(game.difficulty){
        case 1://facile
            game.lifeDragon = getRandomInteger(150, 200);
            game.lifePlayer = getRandomInteger(200, 250);
            break;
        case 2://normal
            game.lifeDragon = getRandomInteger(200, 250);
            game.lifePlayer = getRandomInteger(200, 250);
            break;
        case 3://difficile
            game.lifeDragon = getRandomInteger(200, 250);
            game.lifePlayer = getRandomInteger(150, 200);
            break;
    }

    // le ratio d'attaque du chevalier en fonction de son épée
    switch (game.sword) {
        case 1:// épée en bois
            game.swordRatio = 0.5;
            break;
        case 2:// épée en acier
            game.swordRatio = 1;
            break;
        case 3:// épée en acier
            game.swordRatio = 1.5;
            break;                          
       
    }
    // le ratio de l'armure 
    switch (game.armor) {
        case 1:// cuivre
            game.armorRatio = 1;
            break;
        case 2:// fer
            game.armorRatio = 0.75;
            break;
        case 3:// magique
            game.armorRatio = 0.5;
            break;                          
       
    }

    //SIMU : attaque du dragon.
    /* console.log ("TEST - L'attaque du dragon est : " +computeDragonDamagePoint(game.difficulty, game.armorRatio));
    console.log ("TEST - L'attaque du player est : " +computePlayerDamagePoint(game.sword, game.swordRatio));  */        
    console.log("Initialisation\n : ");
    console.log(game);
    return game;
    
 //   console.log(getRandomInteger(150,200)); 
} 
//fonction boucle de la partie
function gameLoop(game){

    let msgFight;
    let randomNumberDragon;// 
    let randomNumberPlayer;
    let roundNumber = 0;// numéro de round
    //la partie continue tant que le joueur et le dragon sont tout les deux en vie
    while(game.lifeDragon > 0 && game.lifePlayer > 0) {
        //attaques :
        msgFight = '';
        game.attackDragon = computeDragonDamagePoint(game.difficulty, game.armorRatio);
        game.attackPlayer = computePlayerDamagePoint(game.sword,game.swordRatio);
        randomNumberDragon = 0;// reinit chque tour 
        randomNumberPlayer = 0;
        //tirage au sort pour déterminer qui jouer en premier  à chaque tour!
        while(randomNumberDragon === randomNumberPlayer){
            
            randomNumberPlayer = getRandomInteger(10,20);
            randomNumberDragon = getRandomInteger(10,20);
            //////////////////DISPLAY//////////////
            console.log("\n///////////ROUND :"+roundNumber+" ///////////");// rappel du round
            //resultat tirage au sort
            console.log("\nPLAYER RANDOM NUMBER: "+randomNumberPlayer+"\nDRAGON RANDOM NUMBER :" +randomNumberDragon);
            //point de vie avant prochain dégat 
            console.log("\nDRAGON LIFE : "+game.lifeDragon+"\nPLAYER LIFE : "+game.lifePlayer);
            //console.log("\nPLAYER LIFE : "+game.lifePlayer);
            ////////////////////////////////////////
        }
        // attaque en fonction de qui tape le premier
        if (randomNumberDragon < randomNumberPlayer ) {//le dragon se fait attaqué         
            game.lifeDragon -= game.attackPlayer;
            msgFight = document.createTextNode("Vous avez été plus rapide, vous infligez : [-"+game.attackPlayer+"]PV au terrible DRAGON\n Il lui reste encore :"+game.lifeDragon+" PV");

        }else{//le joueur se fait attaqué            
            game.lifePlayer -= game.attackDragon;
            msgFight = document.createTextNode("Le DRAGON a été plus rapide que vous. Il vous inflige : [-"+game.attackDragon+"]PV \n Il ne vous reste plus que :"+game.lifePlayer+" PV");
        }
        
        console.log("\n/////////////////FIN DU ROUND////////////////");
        roundNumber++; // nouveau round commence
        // affichage html du combat
        displayFight(game.lifeDragon,game.lifePlayer,roundNumber,msgFight);
       
     }
     console.log("FIN DE LA PARTIE : "); 


     return game;

}

function showWinner(game){

    if (game.lifePlayer <= 0) {
        console.log("Vous avez perdu. Il ne restait seulement : "+game.lifeDragon+" HP !!");       
    } else {
        console.log("Vous avez gagné. Vous avez tué le Dragon !! Il vous reste encore: "+game.lifePlayer+" HP !!");      
 
    }
}

//Lancement du jeu !
startGame();